let express = require('express')
var cors = require('cors')
let app = express()

let path = require('path')
let bodyParser = require('body-parser')
let musicRoute = require('./routes/music')
app.use(cors());
app.use(bodyParser.json())

app.use((req, res, next)=> {
    console.log(`${new Date().toString()}=> ${req.originalUrl}`, req.body)
    next()
})

app.use(musicRoute)
app.use(express.static('public'))
//Handler for 404
app.use((req, res, next) => {
    res.status(404).send('Oh.. 404')
})

// Handler for 500
app.use((err, req, res, next)=>{
    console.error(err.stack)

    res.sendFile(path.join(__dirname,'../public/500.html'))
})
const PORT = process.env.PORT || 5000
app.listen(PORT, () => console.log(`Server has started on ${PORT}`))

