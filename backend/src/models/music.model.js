let mongoose = require('mongoose')

const server = 'ds157528.mlab.com:57528'
const database = 'euterpea'
const user = 'admin'
const password = 'euterpea1'

mongoose.connect(`mongodb://${user}:${password}@${server}/${database}`)

let MusicSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    value: {
        type: Array,
        required: true,
        unique: false
    }
})

module.exports = mongoose.model('Music', MusicSchema)