import React, { Component } from 'react'
import { 
    Modal,
    Input ,
    Header,
    Select,
    Button,
    Label
} from 'semantic-ui-react'
import { instruments } from '../dictionary'
import { SemanticToastContainer, toast } from 'react-semantic-toasts';
import axios from 'axios';
import _ from 'lodash'
export default class EditLineModal extends Component {
    state={
        line: '',
        name: '',
        instrument: ''
    }
    componentDidMount(){
        if(this.props.line){
            this.setState({
                line: this.props.line,
                name: this.props.line.name,
                instrument: this.props.line.instrument
            })
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.line && nextProps.line !== this.state.line){
            this.setState({
                line: nextProps.line,
                name: nextProps.line.name,
                instrument: nextProps.line.instrument
            })
        }
    }

    editLine = () =>{
        var self = this
        this.props.openEditLineModal()
        var songlines = this.props.selectedSongLines
        _.find(songlines, {"name": this.props.line.name}).instrument = this.state.instrument
        _.find(songlines, {"name": this.props.line.name}).name = this.state.name
        axios.put(`http://localhost:5000/music?name=${this.props.selectedSong.name}`, {
          name: this.state.name,
          value: songlines
        })
        .then(function (response) {
          setTimeout(() => {
              toast(
                  {
                      type: 'success',
                      icon: 'check',
                      title: 'Edytowano linię',
                      description: 'Linia '+self.props.line.name+' została edytowana poprawnie',
                      time: 2000
                  }
              );
          }, 20);
          self.props.refreshSongs()
        })
        .catch(function (error) {
          setTimeout(() => {
              toast(
                  {
                      type: 'error',
                      icon: 'exclamation',
                      title: 'Nie edytowano linii',
                      description: 'Nastąpił błąd',
                      time: 4000
                  }
              );
          }, 20);
        });
      }

      handleChangeLineName = (e, { value }) => {
        this.setState({ name: value })
      }

      handleChangeInstrument = (e, { value }) => {
        this.setState({ instrument: value })
      }

    render(){
        return(
            <Modal open={this.props.isEditLineModalOpen}>
            <div style={{padding: 30, display: 'flex', flexDirection: 'column'}}>
                <Header as='h4'>Edytuj Linię</Header>
                <Header as='h5'>Nazwa linii</Header>
                <Input
                    value={this.state.name}
                    onChange={this.handleChangeLineName} 
                />
                <Header as='h5'>Wybierz instrument</Header>
                <Select 
                  placeholder='Select instrument' 
                  value={this.state.instrument} 
                  onChange={this.handleChangeInstrument} 
                  options={instruments} 
                  style={{width: 800}}
                  fluid search selection
                  />
                <Button.Group style={{width: 200, paddingTop: 20}}>
                    <Button onClick={this.props.openEditLineModal()}>Cancel</Button>
                    <Button.Or />
                    <Button positive onClick={()=>this.editLine()}>Save</Button>
                </Button.Group>
                </div>
                <SemanticToastContainer position="bottom-right"/>
            </Modal>
        )
    }
}