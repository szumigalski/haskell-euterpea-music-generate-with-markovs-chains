import React, { Component } from 'react'
import _ from 'lodash'
import {
    Tab,
    Table,
    Button,
    Segment,
    Header,
    Input
} from 'semantic-ui-react'
export default class Statistics extends Component {

    state= {
        tab: [],
        rateTab: [],
        line: '',
        elCounter: '',
        elName: ''
    }
    handleChangeElName = (e, { value }) => {
        this.setState({ elName: value })
      }
    handleChangeElCounter = (e, { value }) => {
        this.setState({ elCounter: value })
      }
componentWillReceiveProps(nextProps){
    if(nextProps.line !== this.state.line) {
        this.createTableFromObject(nextProps.line.soundtable)
        this.createRateTableFromObject(nextProps.line.tempotable)
        this.setState({
            line: nextProps.line
        })
    }
}
    createTableFromObject(object){
        let table = []
        let pomTable = []
        object.map(el =>{
            table.push({
                key: el.name,
                value: []
            })
            pomTable.push({
                key: el.name,
                value: 0
            })
        })
        
        table.map(el=>{
            el.value = JSON.parse(JSON.stringify(pomTable))
        })
        
        object.map(el=>{
            el.table.map(i=>{
                if(_.find(_.find(table, {'key': el.name}).value, {'key': i }) ){
                     _.find(_.find(table, {'key': el.name}).value, {'key': i }).value += 1
                }
            }
            )
        })
        this.setState({
            tab : table
        })
    }

    createRateTableFromObject(object){
        let table = []
        let pomTable = []
        object.map(el =>{
            table.push({
                key: el.name,
                value: []
            })
            pomTable.push({
                key: el.name,
                value: 0
            })
        })
        
        table.map(el=>{
            el.value = JSON.parse(JSON.stringify(pomTable))
        })
        
        object.map(el=>{
            el.table.map(i=>{
                if(_.find(_.find(table, {'key': el.name}).value, {'key': i }) ){
                     _.find(_.find(table, {'key': el.name}).value, {'key': i }).value += 1
                }
            }
            )
        })
        this.setState({
            rateTab : table
        })
    }
    render(){
        const panes = [
            { menuItem: 'Sound table', render: () => 
                <Tab.Pane>
                    <Table definition>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell />
                                {this.props.line && this.props.line.soundtable.map(sound=>
                                <Table.HeaderCell>{sound.name}</Table.HeaderCell>)}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                        {this.state.tab && this.state.tab.map(row=>
                            <Table.Row>
                                <Table.Cell>{row.key}</Table.Cell>
                                {row.value.map(el=>
                                    <Table.Cell>{el.value}</Table.Cell>
                                )}
                            </Table.Row>
                            )}
                        </Table.Body>
                    </Table>
                </Tab.Pane> 
            },
            { menuItem: 'Rate table', render: () => 
                <Tab.Pane>
                    <Table definition>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell />
                                {this.props.line && this.props.line.tempotable.map(rate=>
                                <Table.HeaderCell>{rate.name}</Table.HeaderCell>)}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                        {this.state.rateTab && this.state.rateTab.map(row=>
                            <Table.Row>
                                <Table.Cell>{row.key}</Table.Cell>
                                {row.value.map(el=>
                                    <Table.Cell>{el.value}</Table.Cell>
                                )}
                            </Table.Row>
                            )}
                        </Table.Body>
                    </Table>
                </Tab.Pane> }
          ]
          const panes2 = [
            { menuItem: 'Sound table', render: () => 
                <Tab.Pane>
                    <Table definition>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell />
                                {this.props.projectElementSoundMap && this.props.projectElementSoundMap.map(sound=>
                                <Table.HeaderCell>{sound.name}</Table.HeaderCell>)}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                        {this.state.tab && this.props.tab.map(row=>
                            <Table.Row>
                                <Table.Cell>{row.key}</Table.Cell>
                                {row.value.map(el=>
                                    <Table.Cell>{el.value}</Table.Cell>
                                )}
                            </Table.Row>
                            )}
                        </Table.Body>
                    </Table>
                </Tab.Pane> 
            },
            { menuItem: 'Rate table', render: () => 
                <Tab.Pane>
                    <Table definition>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell />
                                {this.props.projectElementRateMap && this.props.projectElementRateMap.map(rate=>
                                <Table.HeaderCell>{rate.name}</Table.HeaderCell>)}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                        {this.props.rateTab && this.props.rateTab.map(row=>
                            <Table.Row>
                                <Table.Cell>{row.key}</Table.Cell>
                                {row.value.map(el=>
                                    <Table.Cell>{el.value}</Table.Cell>
                                )}
                            </Table.Row>
                            )}
                        </Table.Body>
                    </Table>
                </Tab.Pane> },
            { menuItem: 'Project Lines', render: () => 
            <Tab.Pane>
                <div style={{display: 'flex', justifyContent: 'space-around'}}>
                    <Table celled style={{width: '60%', marginRight: 5}}>
                        <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Linia</Table.HeaderCell>
                            <Table.HeaderCell>Usuń</Table.HeaderCell>
                        </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {this.props.projectElementList && this.props.projectElementList.map(project=>
                                <Table.Row>
                                    <Table.Cell>
                                        {project.name}
                                    </Table.Cell>
                                    <Table.Cell style={{width: 50}}>
                                        <Button 
                                            circular 
                                            color='red'
                                            icon='trash' 
                                            onClick={()=>this.props.deleteLineFromProject(project)} 
                                            />
                                    </Table.Cell>
                                </Table.Row>
                            )}
                        </Table.Body>
                    </Table>
                    <Segment style={{margin: 0, display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                        <Header as='h4'>Dodaj do projektu</Header>
                        <Button 
                            style={{width: 35}}
                            circular 
                            color='green'
                            icon='plus'
                            onClick={()=>this.props.addProjectElementToProject(this.state.elCounter, this.state.elName)} 
                            />
                        <Header as='h4'>Liczba dźwięków</Header>
                        <Input 
                            type='number'
                            onChange={this.handleChangeElCounter}
                            min={0} 
                            />
                        <Header as='h4'>Nazwa elementu</Header>
                        <Input 
                            onChange={this.handleChangeElName}
                            />
                    </Segment>
                </div>
            </Tab.Pane> },
          ]
        const panes3 = [
            { menuItem: 'Line', render: () => <Tab menu={{ secondary: true, pointing: true }} panes={panes} />  },
            { menuItem: 'Project element', render: () => <Tab menu={{ secondary: true, pointing: true }} panes={panes2} /> }
        ]
        return(
            <Tab menu={{ secondary: true, pointing: true }} panes={panes3} /> 
        )
    }
}