import React, { Component } from 'react'
import { 
    Segment,
    Header,
    Form,
    Radio,
    Select
   } from 'semantic-ui-react'
import { instruments, colorList } from '../dictionary'
export default class ProjectLine extends Component {
    render(){
        return(
            <div style={{
                display: 'flex', 
                alignItems: 'center',  
                marginBottom: 10,
                backgroundColor: this.props.value === this.props.radio ? 'rgba(215,215,215,0.8)' : 'white'
                }}>
                <div style={{width: 250, display: 'flex', alignItems: 'center'}}>
                    <Form.Field>
                            <Radio
                                style={{marginLeft: 2, marginRight: 5}}
                                name='radioGroup'
                                value={this.props.name}
                                checked={this.props.radio ===this.props.name}
                                onChange={this.props.handleChangeRadio()}
                            />
                        </Form.Field>
                    <div style={{fontSize: 20, marginRight: 20}}>{this.props.name}</div>
                    <Select 
                        placeholder='Select instrument' 
                        value={this.props.line} 
                        onChange={this.props.handleChangeInstrumentProject()} 
                        options={instruments} 
                        style={{width: 200}}
                        fluid search selection
                        disabled={!(this.props.radio === this.props.name)}
                        />
                  </div>
                {this.props.elements && this.props.elements.map((el, i)=><Segment 
                    style={{width: el.count/this.props.maxCounter*100+'%', margin: 0}}
                    inverted 
                    color={colorList[i%15]}
                    >
                    {el.name + ' ' + el.count}
                </Segment>)}
            
            </div>
        )
    }
}