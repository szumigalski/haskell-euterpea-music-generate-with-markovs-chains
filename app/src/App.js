import React, { Component } from 'react';
import { 
  Segment,
  Button,
  Image,
  Grid
 } from 'semantic-ui-react'
import axios from 'axios';
import backgroundImg from './img/background.jpg'
import SongTable from './components/SongTable';
import LineTable from './components/LineTable';
import ProjectPlace from './components/ProjectPlace';
import Statistics from './components/Statistics'
import './App.css'
import NewLineModal from './components/NewLineModal';
import EditLineModal from './components/EditLineModal';
import { SemanticToastContainer, toast } from 'react-semantic-toasts';
import _ from 'lodash'
class App extends Component {
  state = {
    song: '',
    prevNote: '',
    note: '',
    prevOctave: '',
    octave: '',
    prevRate: '',
    rate: '',
    rateMap: new Map(),
    soundMap: new Map(),
    instrument: '',
    instrumentLine: '',
    isNewSongModalOpen: false,
    selectedSong: '',
    selectedLine: '',
    ProjectPlaces: [],
    songList2: [],
    lineName: '',
    isNewLineModalOpen: false,
    isEditLineModalOpen: false,
    lineList: [],
    selectedSongLines: [],
    projectElementSoundMap: [],
    projectElementRateMap: [],
    projectElementList: [],
    tab: [],
    rateTab: [],
    songProjectTab: [],
    projectLines: [],
    radio: '',
    maxCounter: 0,
    wholeLine: ''
   }

  componentDidMount() {
    axios.get(`http://localhost:5000/music`)
      .then(res => {
        const music = [] 
        res.data.map(song=>{
          music.push(song)
        });
        this.setState({
          songList2: music
        })
      })
  }

  refreshLine = () => {
    this.setState({
      note: '',
      octave: '',
      rate: '',
      lineList: [],
      instrumentLine: '',
      lineName: '',
      wholeLine: '',
      rateMap: new Map(),
      soundMap: new Map(),
    })
  }

  refreshSongs = () =>{
    axios.get(`http://localhost:5000/music`)
      .then(res => {
        const music = [] 
        res.data.map(song=>{
          music.push(song)
        });
        this.setState({
          songList2: music
        })
      })
  }

  addLineToProject(line) {
    let projectSound = this.state.projectElementSoundMap
    let projectRate = this.state.projectElementRateMap
    let projectList = this.state.projectElementList

    this.state.selectedLine.soundtable.map(sound=>{
      _.find(projectSound, {"name": sound.name}) && (_.find(projectSound, {"name": sound.name}).table = _.find(projectSound, {"name": sound.name}).table.concat(sound.table))
      !_.find(projectSound, {"name": sound.name}) && projectSound.push({name: sound.name, table: sound.table})}
      )
    
    this.state.selectedLine.tempotable.map(tempo=>{
      _.find(projectRate, {"name": tempo.name}) && (_.find(projectRate, {"name": tempo.name}).table = _.find(projectRate, {"name": tempo.name}).table.concat(tempo.table))
      !_.find(projectRate, {"name": tempo.name}) && projectRate.push({name: tempo.name, table: tempo.table})}
      )
    var projectLine = JSON.parse(JSON.stringify(this.state.selectedLine))
    projectLine.name = this.state.selectedSong.name + ' / ' + projectLine.name
    projectList.push(projectLine)
    this.setState({
      projectElementSoundMap: projectSound,
      projectElementRateMap: projectRate,
      projectElementList: projectList
    })

    this.createTableFromObject(projectSound)
    this.createRateTableFromObject(projectRate)
  }

  deleteLineFromProject = (project) =>{
    let projectSound = this.state.projectElementSoundMap
    let projectRate = this.state.projectElementRateMap
    let projectList = this.state.projectElementList

    project.soundtable.map(sound=>
      _.remove(projectSound, {"name": sound.name})
      )
    
      project.tempotable.map(tempo=>
        _.remove(projectSound, {"name": tempo.name})
      )
    projectList.splice( projectList.indexOf(project), 1 );
    this.setState({
      projectElementSoundMap: projectSound,
      projectElementRateMap: projectRate,
      projectElementList: projectList
    })

    this.createTableFromObject(projectSound)
    this.createRateTableFromObject(projectRate)
  }
  selectSong(song){
    this.setState({
      selectedSong: song,
      selectedSongLines: song.value
    })
  }

  selectLine(line){
    this.setState({
      selectedLine: line
    })
  }

  handleChangeNote = (e, { value }) => {
    this.setState({ note: value })
  }
  handleChangeWholeLine = (e, { value }) => {
    this.setState({ wholeLine: value })
  }
  handleChangeLineName = (e, { value }) => {
    this.setState({ lineName: value })
  }

  handleChangeOctave = (e, { value }) => {
    this.setState({ octave: value })
  }

  handleChangeRate = (e, { value }) => {
    this.setState({ rate: value })
  }

  handleChangeFirstNote = (e, { value }) => {
    this.setState({ prevNote: value })
  }

  handleChangeFirstOctave = (e, { value }) => {
    this.setState({ prevOctave: value })
  }

  handleChangeFirstRate = (e, { value }) => {
    this.setState({ prevRate: value })
  }

  handleChangeInstrument = (e, { value }) => {
    this.setState({ instrument: value })
  }

  handleChangeInstrumentLine = (e, { value }) => {
    this.setState({ instrumentLine: value })
  }

  handleChangeInstrumentProject = (ins) => {
    var lines = this.state.projectLines
     _.find(lines, {"name": this.state.radio}).instrument = ins
    this.setState({
      projectLines: lines
    })
  }

  handleChange = (e, { value }) => this.setState({ value })

  setNewRate(){
    let rates = this.state.rateMap
    if(this.state.prevRate !== ''){
      if(rates.has(this.state.prevRate)){
        let tab = rates.get(this.state.prevRate)
        tab.push(this.state.rate)
        rates.set(this.state.prevRate, tab)
      } else {
        rates.set(this.state.prevRate, [this.state.rate])
      }
    }
    this.setState({
      prevRate: this.state.rate
    })
  }

  setNewSound(){
    let sounds = this.state.soundMap
    if(this.state.prevNote === 'rest'){
      var prevSound = this.state.prevNote
    } else{
      var prevSound = this.state.prevNote + ' ' + this.state.prevOctave
    }
    if(this.state.note === 'rest'){
      var sound = this.state.note
    }
    else {
      var sound = this.state.note + ' ' + this.state.octave
      }
    if(this.state.prevNote !== '' && this.state.prevOctave !== ''){
      if(sounds.has(prevSound)){
        let tab = sounds.get(prevSound)
        tab.push(sound)
        sounds.set(prevSound, tab)
      } else {
        sounds.set(prevSound, [sound])
      }
    }
    if(this.state.note !== 'rest'){
      this.setState({
        prevNote: this.state.note,
        prevOctave: this.state.octave,
      })
    } else {
      this.setState({
        prevNote: this.state.note
      })
    }
  }

  addNewSound = () =>{
    this.setNewRate()
    this.setNewSound()
    if(this.state.note !== 'rest'){
      this.state.lineList.push(this.state.note + ' ' + this.state.octave + ' ' + this.state.rate)
      }
    else{
      this.state.lineList.push(this.state.note + ' ' + this.state.rate)
    }
  }

  setNewLineRekurencja(prevN, prevO, prevR, tab, mapS, mapR){
    console.log(prevN, prevO, prevR, tab, mapS, mapR)
    if(!tab.length){
      this.setState({
        prevNote: prevN,
        prevOctave: prevO,
        prevNote: prevR,
      })
    } else {
      if(prevN === 'rest'){
        var prevS = prevN
      } else {
        var prevS = prevN + ' ' + prevO
      }
        if(tab[0] === 'rest'){
          if(mapS.has(prevS)){
              let t = mapS.get(prevS)
              t.push(tab[0])
              mapS.set(prevS, t)
            } else {
              mapS.set(prevS, [tab[0]])
            }
          if(mapR.has(prevR)){
              let t = mapR.get(prevR)
              t.push(tab[1])
              mapR.set(prevR, t)
            } else {
              mapR.set(prevR, [tab[1]])
            }
          this.state.lineList.push(tab[0] + ' ' + tab[1])
          var pN = tab.shift()
          var pR = tab.shift()
          tab.shift()
          return this.setNewLineRekurencja(pN, '', pR, tab, mapS, mapR)
        } else {
          if(mapS.has(prevS)){
            let t = mapS.get(prevS)
            t.push(tab[0]+ ' ' + tab[1])
            mapS.set(prevS, t)
          } else {
            mapS.set(prevS, [tab[0]+ ' ' + tab[1]])
          }
          if(mapR.has(prevR)){
              let t = mapR.get(prevR)
              t.push(tab[2])
              mapR.set(prevR, t)
            } else {
              mapR.set(prevR, [tab[2]])
            }
          this.state.lineList.push(tab[0] + ' ' + tab[1] + ' ' + tab[2])
          var pN = tab.shift()
          var pO = tab.shift()
          var pR = tab.shift()
          tab.shift()
          return this.setNewLineRekurencja(pN, pO, pR, tab, mapS, mapR)
        }
      }
  }

  addNewSoundLine = () =>{
    var line = this.state.wholeLine.split(' ')
    console.log('lol', line)
    if(this.state.prevNote === ''){
      if(line[1] === 'rest'){
        var pN = line.shift()
        var pR = line.shift()
        line.shift()
        return this.setNewLineRekurencja(pN, '', pR, line, this.state.soundMap, this.state.rateMap)
      } else {
        var pN = line.shift()
        var pO = line.shift()
        var pR = line.shift()
        line.shift()
        return this.setNewLineRekurencja(pN, pO, pR, line, this.state.soundMap, this.state.rateMap)
      }
    } else {
      if(this.state.prevNote === 'rest'){
        var pN = this.state.prevNote
        var pR = this.state.prevRate
        return this.setNewLineRekurencja(pN, '', pR, line, this.state.soundMap, this.state.rateMap)
      } else {
        var pN = this.state.prevNote
        var pO = this.state.prevOctave
        var pR = this.state.prevRate
        return this.setNewLineRekurencja(pN, pO, pR, line, this.state.soundMap, this.state.rateMap)
      }
    }
      

  }

  addSoundToSong=(counter, sound, rate, song)=>{
    if (counter <= 0) {
      return song
    }
    else{
      var randSound = _.find(this.state.projectElementSoundMap, {"name": sound}).table[Math.floor(Math.random() * _.find(this.state.projectElementSoundMap, {"name": sound}).table.length)]
      var randRate = _.find(this.state.projectElementRateMap, {"name": rate}).table[Math.floor(Math.random() * _.find(this.state.projectElementRateMap, {"name": rate}).table.length)]
      song += randSound + ' ' + randRate + ' :+: '
      return this.addSoundToSong(counter -1, randSound, randRate, song)
    }
  }
  
  createLine = () =>{
    this.openNewLineModal()
    var self = this
    let lineSoundTable = []
    let lineTempoTable = []
    for (let [k, v] of this.state.soundMap) {
      lineSoundTable.push(
        {
          name: k,
          table: v
        })
    }
    for (let [k, v] of this.state.rateMap) {
      lineTempoTable.push(
        {
          name: k,
          table: v
        })
    }
    var songlines = this.state.selectedSongLines
    songlines.push(
      {
        name: self.state.lineName,
        instrument: self.state.instrumentLine,
        soundtable: lineSoundTable,
        tempotable: lineTempoTable
      }
    )
    axios.put(`http://localhost:5000/music?name=${this.state.selectedSong.name}`, {
      value: songlines
    })
    .then(function (response) {
      setTimeout(() => {
          toast(
              {
                  type: 'success',
                  icon: 'check',
                  title: 'Dodano linię',
                  description: 'Linia '+self.state.lineName+' została dodana poprawnie',
                  time: 2000
              }
          );
          self.setState({
            lineName: ''
          })
      }, 20);
      self.refreshSongs()
    })
    .catch(function (error) {
      setTimeout(() => {
          toast(
              {
                  type: 'error',
                  icon: 'exclamation',
                  title: 'Nie dodano linii',
                  description: 'Nastąpił błąd',
                  time: 4000
              }
          );
      }, 20);
    });
  }

  deleteLine = (lineName) =>{
    var self= this
    var lines = this.state.selectedSongLines
    _.remove(lines , _.find(lines, { 'name': lineName }))
    axios.put(`http://localhost:5000/music?name=${this.state.selectedSong.name}`, {
      value: lines
    })
    .then(function (response) {
      setTimeout(() => {
          toast(
              {
                  type: 'success',
                  icon: 'check',
                  title: 'Usunięto linię',
                  description: 'Linia '+lineName+' została dodana poprawnie',
                  time: 2000
              }
          );
      }, 20);
      self.refreshSongs()
    })
    .catch(function (error) {
      setTimeout(() => {
          toast(
              {
                  type: 'error',
                  icon: 'exclamation',
                  title: 'Nie usunięto linii',
                  description: 'Nastąpił błąd',
                  time: 4000
              }
          );
      }, 20);
    });
  }

  openNewLineModal = () => {
    this.state.isNewLineModalOpen && this.refreshLine()
    this.setState({
      isNewLineModalOpen: !this.state.isNewLineModalOpen
    })
  }

  openEditLineModal=()=>{
    this.setState({
      isEditLineModalOpen: !this.state.isEditLineModalOpen
    })
  }

  createTableFromObject=(object)=>{
    let table = []
    let pomTable = []
    object.map(el =>{
        table.push({
            key: el.name,
            value: []
        })
        pomTable.push({
            key: el.name,
            value: 0
        })
    })
    
    table.map(el=>{
        el.value = JSON.parse(JSON.stringify(pomTable))
    })
    
    object.map(el=>{
        el.table.map(i=>{
            if(_.find(_.find(table, {'key': el.name}).value, {'key': i }) ){
                 _.find(_.find(table, {'key': el.name}).value, {'key': i }).value += 1
            }
        }
        )
    })
    this.setState({
        tab : table
    })
}
createRateTableFromObject=(object)=>{
  let table = []
  let pomTable = []
  object.map(el =>{
      table.push({
          key: el.name,
          value: []
      })
      pomTable.push({
          key: el.name,
          value: 0
      })
  })
  
  table.map(el=>{
      el.value = JSON.parse(JSON.stringify(pomTable))
  })
  
  object.map(el=>{
      el.table.map(i=>{
          if(_.find(_.find(table, {'key': el.name}).value, {'key': i }) ){
               _.find(_.find(table, {'key': el.name}).value, {'key': i }).value += 1
          }
      }
      )
  })
  this.setState({
      rateTab : table
  })
}
handleChangeRadio = (name)=>{
  this.setState({
    radio: name
  })
}
  addProjectElementToProject = (c, elName) =>{
    var prTab = this.state.projectLines
    var randSound = this.state.projectElementSoundMap[Math.floor(Math.random() * this.state.projectElementSoundMap.length)]
    var randRate = this.state.projectElementRateMap[Math.floor(Math.random() * this.state.projectElementRateMap.length)]
    _.find(prTab, {"name": this.state.radio}).elements.push
      ({
        name: elName, 
        value: this.addSoundToSong(c, 
          randSound.table[Math.floor(Math.random() * randSound.table.length)], 
          randRate.table[Math.floor(Math.random() * randRate.table.length)], 
          '' ), 
        count: Number(c)
      })
      _.find(prTab, {"name": this.state.radio}).counter += Number(c)
      let count = _.find(prTab, {"name": this.state.radio}).counter
      if(count > this.state.maxCounter){
        this.setState({
          maxCounter: count
        })
      }
    this.setState({
      songProjectTab: prTab
    })
  }

  downloadHaskell = () =>{
    var fileDownload = require('js-file-download')
    var data = 'import Euterpea\n'
    var music = ''
    this.state.projectLines.map(line=>
      {var row = line.name + ' = '
      if(line.instrument){
        row += ('instrument '+line.instrument+'(')
      }
      line.elements.map(el=>{
        row += el.value
      })
      row=row.substring(0, row.length-4)
      if(line.instrument) row += '))'
      row += '\n'
      data += row
      music += line.name + ' :=: '
    })
    data += 'music = ' + music.substring(0, music.length-4)
    var file = new Blob(
      [data]
      ,{ type: 'plain/text',endings: 'native'})
    fileDownload( file, 'filename.hs')
  }

  addProjectLine = (names) =>{
    var lines = JSON.parse(JSON.stringify(this.state.projectLines))
    var check = lines.length > 0 ? true : false
    lines.push({name: names, elements: [], instrument: '', counter: 0 })
    if(check){
      this.setState({
        projectLines: lines
      })
    } else
      this.setState({
        projectLines: lines,
        radio: names
      })
  }

  render() {
    return (
      <div>
        <Image src={backgroundImg} width='100%' height='100%' style={{zIndex: 1, position: 'absolute'}}/>
        <div 
          style={{
            zIndex: 2, 
            position: 'absolute', 
            margin: 20, 
            width: 'calc(100% - 40px)', 
            height: 'calc(100% - 40px)',
            }}>
            <Grid style={{height: '100%'}}>
              <Grid.Row columns={3} style={{height: '70%'}}>
                <Grid.Column style={{overflowY: 'scroll', maxWidth: '25%'}}>
                  <SongTable 
                    refreshSongs={this.refreshSongs}
                    songList={this.state.songList2}
                    selectedSong={this.state.selectedSong}
                    selectSong={this.selectSong.bind(this)}
                    />
                </Grid.Column>
                <Grid.Column style={{overflowY: 'scroll', overflowX: 'scroll', maxWidth: '30%'}}>
                  <Button 
                    positive 
                    style={{position: 'relative'}}
                    onClick={()=>this.openNewLineModal()}>
                    Dodaj linię
                  </Button> 
                  <LineTable 
                    song={this.state.selectedSong}
                    selectedLine={this.state.selectedLine}
                    selectLine={this.selectLine.bind(this)}
                    addLineToProject={this.addLineToProject.bind(this)}
                    deleteLine={this.deleteLine.bind(this)}
                    openEditLineModal={this.openEditLineModal.bind(this)}
                    />
                </Grid.Column>
                <Grid.Column style={{overflowY: 'scroll',minWidth: '45%'}}>
                  <Statistics 
                    line={this.state.selectedLine}
                    projectElementSoundMap={this.state.projectElementSoundMap}
                    projectElementRateMap={this.state.projectElementRateMap}
                    projectElementList={this.state.projectElementList}
                    tab={this.state.tab}
                    rateTab={this.state.rateTab}
                    deleteLineFromProject={this.deleteLineFromProject.bind(this)}
                    addProjectElementToProject={this.addProjectElementToProject.bind(this)}
                    />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row columns={1} style={{height: '30%'}}>
                <Grid.Column>
                  <ProjectPlace 
                    radio={this.state.radio}
                    projectLines={this.state.projectLines}
                    downloadHaskell={this.downloadHaskell.bind(this)}
                    addProjectLine={this.addProjectLine.bind(this)}
                    handleChangeRadio={this.handleChangeRadio.bind(this)}
                    handleChangeInstrumentProject={this.handleChangeInstrumentProject.bind(this)}
                    maxCounter={this.state.maxCounter}
                    />
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <NewLineModal
              isNewLineModalOpen = {this.state.isNewLineModalOpen}
              openNewLineModal = {()=>this.openNewLineModal}
              handleChangeLineName={()=>this.handleChangeLineName}
              lineName={this.state.lineName}
              handleChangeInstrumentLine={()=>this.handleChangeInstrumentLine}
              prevNote = {this.state.prevNote}
              note = {this.state.note}
              prevOctave={this.state.prevOctave}
              octave={this.state.octave}
              prevRate={this.state.prevRate}
              rate={this.state.rate}
              rateMap={this.state.rateMap}
              soundMap={this.state.soundMap}
              instrument={this.state.instrument}
              handleChangeFirstNote={()=>this.handleChangeFirstNote}
              handleChangeNote={()=>this.handleChangeNote}
              handleChangeFirstOctave={()=>this.handleChangeFirstOctave}
              handleChangeFirstRate={()=>this.handleChangeFirstRate}
              handleChangeOctave={()=>this.handleChangeOctave}
              handleChangeRate={()=>this.handleChangeRate}
              addNewSound={()=>this.addNewSound}
              handleChangeInstrument={()=>this.handleChangeInstrument}
              lineList={this.state.lineList}
              createLine={()=>this.createLine}
              handleChangeWholeLine={()=>this.handleChangeWholeLine}
              addNewSoundLine={()=>this.addNewSoundLine}
              />
              <EditLineModal
                isEditLineModalOpen = {this.state.isEditLineModalOpen}
                openEditLineModal = {()=>this.openEditLineModal}
                handleChangeLineName={()=>this.handleChangeLineName}
                lineName={this.state.lineName}
                handleChangeInstrumentLine={()=>this.handleChangeInstrumentLine}
                instrument={this.state.instrument}
                lineList={this.state.lineList}
                editLine={()=>this.editLine}
                line={this.state.selectedLine}
                selectedSong={this.state.selectedSong}
                selectedSongLines={this.state.selectedSongLines}
                refreshSongs={()=>this.refreshSongs}
                />
              <SemanticToastContainer position="bottom-right"/>
        </div>
      </div>
      )}
}

export default App;
