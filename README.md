# Inpiracja

Projekt inspirowany tym o to wykładem. Polecam obejrzeć, aby zrozumieć co tu się dzieje

[![Muzyka generowana](http://img.youtube.com/vi/pxvp94KQqeI/0.jpg)](https://www.youtube.com/watch?v=pxvp94KQqeI "Muzyka generowana")

# Piosenka -> tabela

Pierwszym krokiem będzie przekształcenie nut w dwie tabele. Pierwsza będzie przechowywać wysokość nut, a druga ich długość (dane będą od siebie niezależne,  
możliwe, że zrobienie tego w jednej tabeli da jeszcze lepszy efekt).  
  
## Przykładowe nuty  
  
[Link do nut - Horse racing](https://musescore.com/user/7077246/scores/1928676)  

## Tabele stworzone z nut (mogą być błędy, szczególnie w pierwszej)
  
[Tabela wysokości nut](https://gitlab.com/Szumigalski/haskell-euterpea-music-generate-with-markovs-chains/blob/master/img/nuty1.png)
  
[Tabela tempa](https://gitlab.com/Szumigalski/haskell-euterpea-music-generate-with-markovs-chains/blob/master/img/nuty2.png)  
  
*wiem, jestem za głupi na import obrazków w markdownie, ale robię to pół godziny po północy*

## Tworzymy automat 

Użyłem do tego JS ponieważ najlepiej czuję się w tym języku pewnie dałoby się w Haskellu, ale tak było dla mnie szybciej.  
Nie będę się zagłębiał w szczegóły, myślę, że jeśli ktoś oglądał i zrozumiał wykład nie powinien mieć problemów z tym kodem.

## Hop do Haskella

Wygenerowany przez JS kod można spokojnie przekleić do Haskella, należy pamiętać o zbędnym **:+:** na początku utworu (wiem, niechluj ze mnie) 
i o dodaniu **import Eutepea** na początku utworu

# Aplikacja do generowania muzyki

Zainspirowany efektami jakie daje generowanie muzyki postanowiłem stworzyć aplikację webową, która pozwala na generowanie własnych utworów z tych już instniejących.  
Do tego użyłem bazy danych *MongoDB* a front jest napisany w *React JS*, do stylowania użyłem *Semantic UI*. 

## Jak to działa

Omówię po kolei elementy strony - na początku widzimy tabelkę z nazwami piosenek. Tutaj operujemy tylko na nazwach utworów, całe ciało piosenek znajduje się w  
kolejnej tabeli zawierającej *linie* utworu. Każda linia zawiera swoją *nazwę*, nazwę *instrumentu*, oraz dźwięki z których się składa. Każdy dźwięk składa się z dobrze  
znanych z Haskella elementów, czyli *nazwa dźwięku*, *wysokość dźwięku*, oraz *długość dźwięku*. Ostatnia część zawiera widok tabel dźwięków, oraz listę linii wchodzących w  
skład *elementu projektu*. 

## Projekt

Mówiąc o *elementach projektu* mam na myśli kawałek *linii projektu*. Projekt to tak naprawdę nowa piosenka do której dodajemy nowe linie, z tą różnicą, że nie wprowadzamy  
tu ręcznie dźwięków, tylko generujemy je z już istniejących linii. Możemy sobie wybrać długość wygenerowanego dźwięku. Każda linia ma też oczywiście swój instrument

## Eksport

Projekty nie są już zapisywane w bazie danych, ale możemy je zapisać w sowim komputerze generując sobie plik Haskellowy z projektu